import json
from datetime import datetime
from pymongo import MongoClient
import paho.mqtt.client as paho
from paho import mqtt
from pymongo.errors import ConnectionFailure
import os
from dotenv import load_dotenv
load_dotenv()
import sys
print("Some log message")

# ... same function definitions as before ...

# MongoDB configuration
clientURL = os.getenv('MONGODB_URL')  # replace with your MongoDB URL
client = os.getenv('MONGO_CLIENT')  # replace with your database name
clientId = os.getenv('MQTT_CLIENT_ID')  # replace with your client id
print("MQTT Client ID:", clientId)  # This will print the client ID to the logs
mongoCollection = os.getenv('MONGO_COLLECTION')  # replace with your collection name
#print all the environment variables
print("MONGODB_URL:", clientURL)
print("MONGO_CLIENT:", client)
print("MQTT_CLIENT_ID:", clientId)
print("MONGO_COLLECTION:", mongoCollection)
print("BROKER_ADDRESS:", os.getenv('BROKER_ADDRESS'))
print("BROKER_PORT:", os.getenv('BROKER_PORT'))
print("BROKER_USER:", os.getenv('BROKER_USER'))
print("BROKER_PASS:", os.getenv('BROKER_PASS'))
mongo_client = MongoClient(clientURL)
db = mongo_client[client]  # replace with your database name
collection = db[mongoCollection]  # replace with your collection name


broker_address = os.getenv('BROKER_ADDRESS')
broker_port = int(os.getenv('BROKER_PORT'))
broker_user = os.getenv('BROKER_USER')
broker_password = os.getenv('BROKER_PASS')
mqtt_client = mqtt_client = paho.Client(client_id=clientId, protocol=paho.MQTTv311)  # replace with a unique client id
topic = os.getenv('MQTT_TOPIC')
def on_connect(client, userdata, flags, rc, properties=None):
    print("CONNACK received with code %s." % rc)
    sys.stdout.flush()

# with this callback you can see if your publish was successful


def on_publish(client, userdata, mid, properties=None):
    print("mid: " + str(mid))

# print which topic was subscribed to


def on_subscribe(client, userdata, mid, granted_qos, properties=None):
    print("Subscribed to topic: "+ str(topic)  +" " + str(mid) + " " + str(granted_qos))
    sys.stdout.flush()


def on_message(client, userdata, message):
    print("Received message: ", str(message.payload.decode("utf-8")))
    sys.stdout.flush()
    try:
        data = json.loads(message.payload.decode())  # parse the JSON payload
        data["timestamp"] = datetime.now()  # add a timestamp
        if(message.topic == topic):
            result = collection.insert_one(data)
        else:
            print("Topic not found")

        # add a timestamp to the data
       
        print("Insertion result: ", result.inserted_id)
        sys.stdout.flush()
    except json.JSONDecodeError:
        print("Failed to parse JSON: ", message.payload.decode())
        sys.stdout.flush()


mqtt_client.on_connect = on_connect
mqtt_client.on_publish = on_publish
mqtt_client.on_subscribe = on_subscribe
# mqtt_client.tls_set(tls_version=mqtt.client.ssl.PROTOCOL_TLS)
# Set broker username and password
mqtt_client.on_message = on_message  # set the callback function
mqtt_client.username_pw_set(broker_user, broker_password)

print("Connecting to MQTT Broker...")
try:
    # Connect to MQTT Broker
    
    mqtt_client.connect(broker_address, broker_port)
    mqtt_client.subscribe(topic, qos=0)
    print("Connected to MQTT Broker")
except Exception as e:
    print("Failed to connect to MQTT Broker:", e)
    # Exit or continue based on your preference
    exit(1)  # Exit the script with an error code
try:
    # Check MongoDB connection
    print("Checking MongoDB connection...")
    mongo_client.admin.command('ismaster')
    print("MongoDB is connected!")
except ConnectionFailure:
    print("MongoDB is not connected!")
    # Exit or handle reconnection logic
    exit(1)  # Exit the script with an error code


try:
    # Start the MQTT loop
    print("Starting MQTT loop...")
    sys.stdout.flush()
    mqtt_client.loop_forever()
except KeyboardInterrupt:
    print("Script interrupted by user, cleaning up...")
except Exception as e:
    print("An error occurred in the MQTT loop:", e)
finally:
    # Disconnect MQTT client and close MongoDB client
    print("Disconnecting...")
    mqtt_client.disconnect()
    mongo_client.close()
    print("Disconnected.")
